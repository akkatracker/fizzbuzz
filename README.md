A simple Python script that prints the numbers from 1 to 100, anything divisible by 3 is replaced by "fizz" and 5 by "buzz". If divisible by both then it is replaced by "FizzBuzz"
